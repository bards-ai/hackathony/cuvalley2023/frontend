import React from 'react';
import { Link } from 'react-router-dom';
import LineChart from '../../charts/LineChart01';
import Icon from '../../images/icon-01.svg';
import EditMenu from '../EditMenu';

// Import utilities
import { tailwindConfig, hexToRGB } from '../../utils/Utils';
import { MapContainer, TileLayer, useMap, Popup } from 'react-leaflet'
import { MarkerLayer, Marker } from "react-leaflet-marker";
import { RiWaterFlashFill } from 'react-icons/ri';

function hexMix(colorFrom, colorTo, ratio) {
    const hex = function (x) {
        x = x.toString(16);
        return (x.length == 1) ? '0' + x : x;
    };

    let r = Math.ceil(parseInt(colorTo.substring(0, 2), 16) * ratio + parseInt(colorFrom.substring(0, 2), 16) * (1 - ratio)),
        g = Math.ceil(parseInt(colorTo.substring(2, 4), 16) * ratio + parseInt(colorFrom.substring(2, 4), 16) * (1 - ratio)),
        b = Math.ceil(parseInt(colorTo.substring(4, 6), 16) * ratio + parseInt(colorFrom.substring(4, 6), 16) * (1 - ratio));

    return hex(r) + hex(g) + hex(b);
}


function SliderCard() {
    const popups = [
        {
            "position": [51.6668058, 16.0341368],
            "name": "Wrocław",
            "value": 0.8
        },
        {
            "position": [51.5668058, 16.1341368],
            "name": "Srilanka",
            "value": 0.1
        },
        {
            "position": [51.3668058, 16.9341368],
            "name": "Srilanka",
            "value": 0.5
        }
    ]

    const num_days = 31;

    return (
        <div className="p-4 flex flex-col col-span-full xl:col-span-12 bg-white shadow-lg rounded-sm border border-slate-200">
            {/* <header className="px-5 py-4 border-b border-slate-100">
                <h2 className="font-semibold text-slate-800">Dzień Predykcji</h2>
            </header> */}
            {/* Chart built with Chart.js 3 */}
            <div className='grow'>
                <div className="form-range appearance-none w-full">
                    <p className='mb-4'>Aktuany dzień: 01.02.2022</p>
                    <input type="range" list="tickmarks" min="1" max={num_days} className="form-range apperance-none w-full"/>
                        <datalist id="tickmarks">
                            {Array.from(Array(num_days).keys()).map((i) => (
                                <option value={i+1}>DUPA</option>
                            ))}
                        </datalist>
                </div>
            </div>
        </div>
    );
}

export default SliderCard;
