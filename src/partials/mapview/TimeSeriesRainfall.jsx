import React from 'react';

// Import utilities
import { tailwindConfig, hexToRGB } from '../../utils/Utils';
import Chart from "react-apexcharts";


function _get_size_class(props) {
    if (!("size" in props)) {
        return "xl:col-span-6"
    }

    if (props.size == "large") {
        return "xl:col-span-12"
    } else {
        return "xl:col-span-6"
    }
}

function _get_title(props) {
    if (!("title" in props)) {
        return ""
    }

    return props.title
}

function TimeSeriesRainfall(props) {
   
    const base_classes = "flex flex-col col-span-full bg-white shadow-lg rounded-sm border border-slate-200"
    const size_class = _get_size_class(props)

    const classes = base_classes + " " + size_class
    const title = _get_title(props)

    const chart_options = {
        series: [    
        {
          type: 'line',
          name: 'Team B Median',
          data: props.data ? props.data : []
        },
      ],
      options: {
        chart: {
          height: 300,
          type: 'rangeArea',
          animations: {
            speed: 500
          }
        },
        colors: ['#d4526e'],
        dataLabels: {
          enabled: false
        },
        fill: {
          opacity: [1]
        },
        forecastDataPoints: {
          count: 0
        },
        stroke: {
          curve: 'straight',
          width: [2]
        },
        legend: {
            show: false
        },
        markers: {
          hover: {
            sizeOffset: 5
          }
        }
      },
    
    
    };

    return (
        <div className={classes}>
            <header className="px-5 py-4 border-b border-slate-100">
                <h2 className="font-semibold text-slate-800">{title}</h2>
            </header>
            {/* Chart built with Chart.js 3 */}
            <div className='grow' style={{ minHeight: "300px" }}>
                <Chart
                    options={chart_options.options}
                    series={chart_options.series}
                    type="rangeArea"
                    width="100%"
                    height="100%"
                />
            </div>
        </div>
    );
}

export default TimeSeriesRainfall;
