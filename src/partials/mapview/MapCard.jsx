import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import LineChart from '../../charts/LineChart01';
import Icon from '../../images/icon-01.svg';
import EditMenu from '../EditMenu';

// Import utilities
import { tailwindConfig, hexToRGB } from '../../utils/Utils';
import { MapContainer, TileLayer, useMap, Popup, Marker, CircleMarker } from 'react-leaflet'
// import { MarkerLayer, Marker } from "react-leaflet-marker";
import { RiWaterFlashFill } from 'react-icons/ri';
import { getStationValues } from '../../api/map_values';

function hexMix(colorFrom, colorTo, ratio) {
    const hex = function (x) {
        x = x.toString(16);
        return (x.length == 1) ? '0' + x : x;
    };

    let r = Math.ceil(parseInt(colorTo.substring(0, 2), 16) * ratio + parseInt(colorFrom.substring(0, 2), 16) * (1 - ratio)),
        g = Math.ceil(parseInt(colorTo.substring(2, 4), 16) * ratio + parseInt(colorFrom.substring(2, 4), 16) * (1 - ratio)),
        b = Math.ceil(parseInt(colorTo.substring(4, 6), 16) * ratio + parseInt(colorFrom.substring(4, 6), 16) * (1 - ratio));

    return hex(r) + hex(g) + hex(b);
}

function MapCard() {
    const days = [
        "2020-01-01",
        "2020-01-02",
        "2020-01-03",
        "2020-01-04",
        "2020-01-05",
        "2020-01-06",
        "2020-01-07",
        "2020-01-08",
        "2020-01-09",
        "2020-01-10",
    ]

    const num_days = days.length
    const [stationValues, setStationValues] = useState({})
    const [selectedDay, setSelectedDay] = useState(days[0])

    useEffect(() => {
        getStationValues(selectedDay, setStationValues)
    }, [selectedDay])

    const popups = [
        {
            'position': [52.03305555555556, 15.6075],
            'name': 'CIGACICE',
            'value': stationValues['CIGACICE'] || 0
        },
        {
            'position': [51.80222222222222, 15.721666666666668],
            'name': 'NOWA SÓL',
            'value': stationValues['NOWA SÓL'] || 0
        },
        {
            'position': [52.09, 16.644166666666667],
            'name': 'KOŚCIAN',
            'value': stationValues['KOŚCIAN'] || 0
        },
        {
            'position': [51.40888888888889, 16.443333333333335],
            'name': 'ŚCINAWA',
            'value': stationValues['ŚCINAWA'] || 0
        },
        {
            'position': [51.63166666666667, 16.46222222222222],
            'name': 'OSETNO',
            'value': stationValues['OSETNO'] || 0
        },
        {
            'position': [51.782777777777774, 16.66888888888889],
            'name': 'RYDZYNA',
            'value': stationValues['RYDZYNA'] || 0
        },
        {
            'position': [51.67388888888889, 16.059166666666666],
            'name': 'GŁOGÓW',
            'value': stationValues['GŁOGÓW'] || 0
            // 'value': 1
        },
        {
            'position': [51.45361111111112, 16.95611111111111],
            'name': 'KANCLERZOWICE',
            'value': stationValues['KANCLERZOWICE'] || 0
        },
        {
            'position': [51.49305555555556, 17.14638888888889],
            'name': 'ŁĄKI',
            'value': stationValues['ŁĄKI'] || 0
        },
        {
            'position': [51.51611111111111, 17.56416666666667],
            'name': 'BOGDAJ',
            'value': stationValues['BOGDAJ'] || 0
        },
        {
            'position': [51.22611111111112, 16.49277777777778],
            'name': 'MALCZYCE',
            'value': stationValues['MALCZYCE'] || 0
        },
        {
            'position': [51.22777777777778, 16.199166666666667],
            'name': 'PIĄTNICA',
            'value': stationValues['PIĄTNICA'] || 0
        },
        {
            'position': [51.24888888888889, 16.14472222222222],
            'name': 'RZESZOTARY',
            'value': stationValues['RZESZOTARY'] || 0
        },
        {
            'position': [51.13944444444444, 16.025555555555556],
            'name': 'RZYMÓWKA',
            'value': stationValues['RZYMÓWKA'] || 0
        },
        {
            'position': [51.111666666666665, 16.09361111111111],
            'name': 'WINNICA',
            'value': stationValues['WINNICA'] || 0
        },
        {
            'position': [51.04833333333333, 16.189722222222223],
            'name': 'JAWOR',
            'value': stationValues['JAWOR'] || 0
        },
        {
            'position': [51.01777777777777, 15.887777777777778],
            'name': 'ŚWIERZAWA',
            'value': stationValues['ŚWIERZAWA'] || 0
            // 'value': 0.953888
        },
        {
            'position': [51.272777777777776, 15.945555555555556],
            'name': 'CHOJNÓW',
            'value': stationValues['CHOJNÓW'] || 0
        },
        {
            'position': [51.18888888888888, 15.863888888888887],
            'name': 'ZAGRODNO',
            'value': stationValues['ZAGRODNO'] || 0
        },
        {
            'position': [51.26055555555556, 16.726666666666667],
            'name': 'BRZEG DOLNY',
            'value': stationValues['BRZEG DOLNY'] || 0
        },
        {
            'position': [51.08027777777778, 17.71],
            'name': 'NAMYSŁÓW',
            'value': stationValues['NAMYSŁÓW'] || 0
        },
        {
            'position': [51.121944444444445, 16.842777777777776],
            'name': 'JARNOŁTÓW',
            'value': stationValues['JARNOŁTÓW'] || 0
        },
        {
            'position': [51.086666666666666, 16.7925],
            'name': 'BOGDASZOWICE',
            'value': stationValues['BOGDASZOWICE'] || 0
        },
        {
            'position': [50.95305555555556, 16.4925],
            'name': 'ŁAŻANY',
            'value': stationValues['ŁAŻANY'] || 0
            // 'value': 0.936858
        },
        {
            'position': [50.87777777777778, 16.233055555555556],
            'name': 'CHWALISZÓW',
            'value': stationValues['CHWALISZÓW'] || 0
        },
        {
            'position': [50.86027777777778, 16.31361111111111],
            'name': 'ŚWIEBODZICE',
            'value': stationValues['ŚWIEBODZICE'] || 0
        },
        {
            'position': [50.96833333333333, 16.650555555555552],
            'name': 'MIETKÓW',
            'value': stationValues['MIETKÓW'] || 0
            // 'value': 0.878272
        },
        {
            'position': [50.91555555555556, 16.583888888888886],
            'name': 'KRASKÓW',
            'value': stationValues['KRASKÓW'] || 0
            // 'value': 0.863149
        },
        {
            'position': [50.78194444444444, 16.584166666666665],
            'name': 'MOŚCISKO',
            'value': stationValues['MOŚCISKO'] || 0
            // 'value': 0.792339
        },
        {
            'position': [50.72583333333333, 16.64611111111111],
            'name': 'DZIERŻONIÓW',
            'value': stationValues['DZIERŻONIÓW'] || 0
        },
        {
            'position': [50.73305555555556, 16.403333333333332],
            'name': 'JUGOWICE',
            'value': stationValues['JUGOWICE'] || 0
        },
        {
            'position': [51.0375, 16.985],
            'name': 'ŚLĘZA',
            'value': stationValues['ŚLĘZA'] || 0
        },
        {
            'position': [50.80222222222222, 16.892222222222223],
            'name': 'BIAŁOBRZEZIE',
            'value': stationValues['BIAŁOBRZEZIE'] || 0
        },
        {
            'position': [50.94638888888888, 17.30638888888889],
            'name': 'OŁAWA',
            'value': stationValues['OŁAWA'] || 0
        },
        {
            'position': [51.08222222222223, 17.146944444444443],
            'name': 'TRESTNO',
            'value': stationValues['TRESTNO'] || 0
        },
        {
            'position': [50.86555555555556, 17.46944444444444],
            'name': 'BRZEG',
            'value': stationValues['BRZEG'] || 0
            // 'value': 1,
        }]

    return (
        <div className="flex flex-col col-span-full xl:col-span-12 bg-white shadow-lg rounded-sm border border-slate-200">
            <header className="px-5 py-4 border-b border-slate-100">
                <h2 className="font-semibold text-slate-800">Stan Rzeki</h2>
            </header>
            {/* Chart built with Chart.js 3 */}
            <div className='grow'>
                <MapContainer
                    zoomControl={false}
                    touchZoom={false}
                    doubleClickZoom={false}
                    style={{ minHeight: "500px", width: "100%" }} center={[51.6668058, 16.0341368]} zoom={9} scrollWheelZoom={false}>
                    <TileLayer
                        zIndex={0}
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://osm.imgw.pl/hydro/{z}/{x}/{y}.png"
                    />
                    {popups.map((popup, index) => (
                        <CircleMarker
                            center={popup.position}
                            fillOpacity={0.9}
                            fillColor={"#" + hexMix("69D2E7", "FA6900", popup.value)}
                            color={"#" + hexMix("69D2E7", "FA6900", popup.value)}
                        >
                            {/* <Popup>
                                A pretty CSS3 popup. <br /> Easily customizable.
                            </Popup> */}
                        </CircleMarker>
                    ))}

                    {/* <MarkerLayer>
                        {popups.map((popup, index) => (
                            <Marker interactive position={popup.position} size={[10, 10]} placement="center">
                                <div
                                    style={{
                                        boxShadow: "0 0 0 2px white, 0 0 0 4px #" + hexMix("69D2E7", "FA6900", popup.value),
                                        display: "flex",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        backgroundColor: "white",
                                        width: "40px",
                                        height: "40px",
                                        borderRadius: "50%",
                                        fontSize: "1.5rem",
                                        color: "#" + hexMix("69D2E7", "FA6900", popup.value)
                                    }}

                                >
                                    <RiWaterFlashFill />
                                    <p>{popup.name}</p>
                                </div>
                            </Marker>
                        ))}
                    </MarkerLayer> */}

                </MapContainer>
                <div className="form-range appearance-none w-full p-4">
                    <p className='mb-4'>Wybrany dzień: {selectedDay} (odczyty rzeczywiste)</p>
                    <input onChange={e => setSelectedDay(days[e.target.value - 1])} type="range" list="tickmarks" min="1" max={num_days} className="form-range apperance-none w-full" />
                    <datalist id="tickmarks">
                        {Array.from(Array(num_days).keys()).map((i) => (
                            <option key={i} value={i}></option>
                        ))}
                    </datalist>
                </div>
            </div>
        </div>
    );
}

export default MapCard;
