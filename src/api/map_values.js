import {SERVER_URL} from './constants';

// get values from the server
export const getStationValues = (date, callback) => {
    console.log("getStationValues", date)
    fetch(`${SERVER_URL}/station_values?date=${date}`, {"method": "GET", "headers": {"Content-Type": "application/json"}})
        .then(response => response.json())
        .then(data => callback(data))
}