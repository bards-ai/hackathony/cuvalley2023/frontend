import { SERVER_URL } from './constants';

// get values from the server
export const get_predictions = (year, month, day, callback) => {
    console.log("get_predictions", year, month, day)
    fetch(`${SERVER_URL}/predict?` + new URLSearchParams({
        year: year,
        month: month,
        day: day
    }), {
        "method": "GET", "headers": { "Content-Type": "application/json" }
    })
        .then((response) => {
            if (!response.ok) {
                throw new Error(`HTTP error: ${response.status}`);
            }
            return response.json();
        })
        .then((data) => {
            callback(data)
        })
        .catch((error) => {
            console.log("get_predictions", error)
        });
}