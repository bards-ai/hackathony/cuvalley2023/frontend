import { SERVER_URL } from './constants';

// get values from the server
export const get_rainfall = (year, month, day, callback) => {
    console.log("get_rainfall", year, month, day)
    fetch(`${SERVER_URL}/get_rainfall?` + new URLSearchParams({
        year: year,
        month: parseInt(month),
        day: parseInt(day)
    }), {
        "method": "GET", "headers": { "Content-Type": "application/json" }
    })
        .then((response) => {
            if (!response.ok) {
                throw new Error(`HTTP error: ${response.status}`);
            }
            return response.json();
        })
        .then((data) => {
            callback(data)
        })
        .catch((error) => {
            console.log("get_rainfall", error)
            callback([
                {
                    x: 'Jan',
                    y: 1500
                },
                {
                    x: 'Feb',
                    y: 1700
                },
                {
                    x: 'Mar',
                    y: 1900
                },
                {
                    x: 'Apr',
                    y: 2200
                },
                {
                    x: 'May',
                    y: 3000
                },
                {
                    x: 'Jun',
                    y: 1000
                },
                {
                    x: 'Jul',
                    y: 2100
                },
                {
                    x: 'Aug',
                    y: 1200
                }
            ])
        });
}