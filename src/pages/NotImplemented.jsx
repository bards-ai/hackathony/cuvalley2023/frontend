import React, { useState } from 'react';

import Sidebar from '../partials/Sidebar';
import Header from '../partials/Header';
import Banner from '../partials/Banner';

function NotImplemented() {

  const [sidebarOpen, setSidebarOpen] = useState(false);

  return (
    <div className="flex h-screen overflow-hidden">

      {/* Sidebar */}
      <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />

      {/* Content area */}
      <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">

        {/*  Site header */}
        <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />

        <main className='h-full grid place-content-center'>
          <div className="flex flex-col grow px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">
            <h1 className='text-2xl'>Funkcjonalność nie jest jeszcze gotowa</h1>
            <p className='mt-2'>Przepraszamy, ale na udostępnionym etapie prototypu, ta funkcjonalność nie jest jeszcze gotowa.</p>
          </div>
        </main>

        <Banner />

      </div>
    </div>
  );
}

export default NotImplemented;