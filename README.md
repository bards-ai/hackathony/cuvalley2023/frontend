# Frontend

## Projekt

Repozytorium to zawiera interfejs graficzny do przedstawiania analizy wyników modelu uczenia maszynowego oraz statystki danych zaleznych. 

Interfejs pozwala na wybór różnych opcji wizualizacji, takich jak wybór okresu czasowego czy filtrowanie danych. Użytkownik może także porównać przewidywania modelu z rzeczywistymi danymi i ocenić jego skuteczność.

![Alt text](https://media.discordapp.net/attachments/1068603815110131763/1069058273057194005/Zrzut_ekranu_2023-01-29_o_01.55.40.png?width%3D1870%26height%3D721)]


Korelacje poziomu wody z stacją hydrologiczną w Głogowie.

## Wymagania

- npm

## Urchomienie serwera

`npm install`

`npm run dev`


